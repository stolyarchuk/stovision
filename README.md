# Age Detection (OpenCV, Boost)

## Install instructions for Debian 9.0

### Install prerequisites

```bash
sudo apt-get update -y
sudo apt-get install -y build-essential cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev python3-dev python3-numpy libeigen3-dev libjpeg-dev libpng-dev libtiff-dev libdc1394-22-dev libboost-thread-dev libboost-system-dev libboost-signals-dev libboost-log-dev libboost-program-options-dev
```

### Install OpenCV > 4.0

```bash
git clone https://github.com/opencv/opencv.git --depth=1
git clone https://github.com/opencv/opencv_contrib.git --depth=1
cd opencv
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local/opencv4 -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules -D CMAKE_BUILD_TYPE=RELEASE -D BUILD_PNG=ON -D BUILD_JPEG=ON -D BUILD_TIFF=ON -D BUILD_SHARED_LIBS=ON -D BUILD_opencv_legacy=OFF -D BUILD_opencv_apps=OFF -D BUILD_EXAMPLES=OFF -D BUILD_DOCS=OFF -D BUILD_PERF_TESTS=OFF -D BUILD_TESTS=OFF -D PYTHON_DEFAULT_EXECUTABLE=$(which python3) ..
make -j$(nproc)
sudo make install
ln -sf /usr/local/opencv4/lib /usr/local/opencv4/lib64
```

### Install app

```bash
git clone https://git.orglot.office/rstolyarchuk/age-detector.git --depth=1
cd age-detector
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
```

## Run app

```bash
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/opencv4/lib64 age-detector -n /usr/local/share/age-detector/models -v0
```

```bash
Usage: age_detector [options]

  -h [ --help ]                         Print this help message and exit.
  -n [ --net ] arg                      Path to frozen DNNs
  -c [ --confidence ] arg (=0.7)        Face detection confidence
  -m [ --margin ] arg (=0.4)            Face detection crop margin
  -f [ --framerate ] arg (=10)          Camera framerate (default: 10)
  -r [ --resolution ] arg (=1280x720)   Camera resolution (default: 1280x720)
  -t [ --threshold ] arg (=10)          DNN propagation threshold (default: 10)
  -v [ --verbose ] arg (=1)             Logging level (0..4)
```
