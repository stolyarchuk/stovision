#ifndef HUMAN_FACE_H
#define HUMAN_FACE_H

#include <dlib/dnn.h>
#include <opencv2/opencv.hpp>

namespace SL {

class HumanFace {
 public:
  using SharedPtr = std::shared_ptr<HumanFace>;
  using Vector = std::vector<SharedPtr>;
  using Features = dlib::matrix<float, 0, 1>;

  HumanFace(float x1, float y1, float x2, float y2);
  HumanFace(int x1, int y1, int x2, int y2);

  cv::Rect GetMarginRect() const;
  cv::Rect2f GetRawRect() const;
  cv::Mat GetFaceMat(cv::Mat&) const;
  cv::Mat GetFaceWithMargin(cv::Mat&) const;

  int GetAge() const;
  void SetAge(int age);

  void GenColor();
  void Margin(cv::Mat&, float);
  void DrawRect(cv::Mat&);
  void AddAgeLabel(cv::Mat&);

  Features GetFeatures() const;
  void SetFeatures(const Features& features);

 private:
  cv::Scalar color_;
  cv::Rect margin_rect_;
  cv::Rect2f raw_rect_;

  Features features_;

  float x1_, y1_, x2_, y2_;
  int height_, width_;

  int age_;
};

}  // namespace SL

#endif  // HUMAN_FACE_H
