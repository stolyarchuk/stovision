#ifndef SCENE_H
#define SCENE_H

#include <atomic>

#include <boost/signals2.hpp>
#include <opencv2/opencv.hpp>

#include "camera/frame.h"
#include "core/block.h"
#include "core/logger.h"
#include "scene/human_face.h"
#include "scene/human_person.h"

namespace SL {

class Scene : public Block {
 public:
  using SharedPtr = std::shared_ptr<Scene>;
  using UniquePtr = std::unique_ptr<Scene>;

  using Block::Block;
  ~Scene() override;

  void Start() override;
  void Stop() override;

  void AddPerson(HumanPerson::SharedPtr person);
  HumanPerson::Vector GetPersons() const;

  bool IsEmpty() const;
  void Destroy() const;
  void Clear();

  void Merge(Frame::SharedPtr);
  void Render(Frame::SharedPtr);
  bool Check(HumanFace::SharedPtr);

  void ConnectToKeyEscape(int idx, std::function<void()>&& slot);

 private:
  std::atomic_bool stopped_;
  HumanPerson::Vector persons_;
  boost::signals2::signal<void()> on_key_escape_;
};

}  // namespace SL

#endif  // SCENE_H
