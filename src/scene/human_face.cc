#include "human_face.h"

#include <random>

#include "core/logger.h"
#include "core/utils.h"

namespace SL {

HumanFace::HumanFace(float x1, float y1, float x2, float y2) : x1_(x1), y1_(y1), x2_(x2), y2_(y2), age_(-1) {
  width_ = static_cast<int>(x2_ - x1_);
  height_ = static_cast<int>(y2_ - y1_);

  raw_rect_ = cv::Rect2f(cv::Point2f(x1, y1), cv::Point2f(x2, y2));
  GenColor();
}

HumanFace::HumanFace(int x1, int y1, int x2, int y2) : x1_(x1), y1_(y1), x2_(x2), y2_(y2), age_(-1) {
  width_ = static_cast<int>(x2_ - x1_);
  height_ = static_cast<int>(y2_ - y1_);

  raw_rect_ = cv::Rect2f(cv::Point2f(x1, y1), cv::Point2f(x2, y2));
  GenColor();
}

void HumanFace::DrawRect(cv::Mat& mat) {
  cv::rectangle(mat, raw_rect_, color_, 2, 4);

  if (age_ > 0)
    AddAgeLabel(mat);
}

void HumanFace::AddAgeLabel(cv::Mat& mat) {
  std::string age_str = std::to_string(age_);
  auto text_size = cv::getTextSize(age_str, cv::FONT_HERSHEY_SIMPLEX, 1, 2, nullptr);

  cv::rectangle(mat, cv::Point2f(raw_rect_.x, raw_rect_.y - text_size.height),
                cv::Point2f(raw_rect_.x + text_size.width, raw_rect_.y), color_, cv::FILLED);

  cv::putText(mat, age_str,                           //
              cv::Point2f(raw_rect_.x, raw_rect_.y),  //
              cv::FONT_HERSHEY_SIMPLEX, 1,            //
              cv::Scalar(255, 255, 255), 2);
}

HumanFace::Features HumanFace::GetFeatures() const {
  return features_;
}

void HumanFace::SetFeatures(const Features& features) {
  features_ = features;
}

cv::Rect HumanFace::GetMarginRect() const {
  return margin_rect_;
}

cv::Rect2f HumanFace::GetRawRect() const {
  return raw_rect_;
}

cv::Mat HumanFace::GetFaceMat(cv::Mat& mat) const {
  cv::Mat face_mat = mat(raw_rect_);
  return face_mat;
}

cv::Mat HumanFace::GetFaceWithMargin(cv::Mat& mat) const {
  cv::Mat face_with_margin_mat = mat(margin_rect_);
  return face_with_margin_mat;
}

int HumanFace::GetAge() const {
  return age_;
}

void HumanFace::SetAge(int age) {
  age_ = age;
}

void HumanFace::Margin(cv::Mat& mat, float margin) {
  int face_width = static_cast<int>(x2_ - x1_);
  int face_height = static_cast<int>(y2_ - y1_);

  auto xw1 = std::max(static_cast<int>((x1_ - margin * face_width)), 0);
  auto yw1 = std::max(static_cast<int>((y1_ - margin * face_height)), 0);
  auto xw2 = std::min(static_cast<int>((x2_ + margin * face_width)), mat.cols);
  auto yw2 = std::min(static_cast<int>((y2_ + margin * face_height)), mat.rows);

  margin_rect_ = cv::Rect(cv::Point(xw1, yw1), cv::Point(xw2, yw2));
}

void HumanFace::GenColor() {
  color_ = cv::Scalar(255, 0, 0);
}

}  // namespace SL
