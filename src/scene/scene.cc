#include "scene.h"

#include <dlib/dnn.h>

#include "core/logger.h"

namespace SL {

Scene::~Scene() {
  cv::destroyAllWindows();
}

void Scene::Start() {
  stopped_ = false;
  cv::namedWindow("human_scene", cv::WINDOW_AUTOSIZE);
  on_key_escape_.connect([&]() { cv::destroyAllWindows(); });

  LOG(INFO) << "Scene module initialized";
}

void Scene::Stop() {
  stopped_ = true;
}

void Scene::AddPerson(HumanPerson::SharedPtr person) {
  persons_.push_back(person);
}

HumanPerson::Vector Scene::GetPersons() const {
  return persons_;
}

bool Scene::IsEmpty() const {
  return persons_.empty();
}

void Scene::Destroy() const {
  cv::destroyAllWindows();
}

void Scene::Render(Frame::SharedPtr frame) {
  if (stopped_)
    return;

  cv::imshow("human_scene", frame->Mat());

  if (cv::waitKey(30) >= 0) {
    on_key_escape_();
  }
}

bool Scene::Check(HumanFace::SharedPtr face) {
  for (auto& person : persons_) {
    if (dlib::length(face->GetFeatures() - person->GetFace()->GetFeatures()) < 0.6) {
      person->SetFace(face);
      return true;
    }
  }

  return false;
}

void Scene::Clear() {
  persons_.clear();
}

void Scene::ConnectToKeyEscape(int idx, std::function<void()>&& slot) {
  on_key_escape_.connect(idx, std::move(slot));
}

void Scene::Merge(Frame::SharedPtr frame) {
  if (stopped_)
    return;

  LOG(DEBUG) << "total faces on frame: " << frame->GetFaces().size();

  for (auto& face : frame->GetFaces()) {
    if (!Check(face)) {
      auto person = std::make_shared<HumanPerson>(face);
      persons_.push_back(person);
    }
  }

  LOG(DEBUG) << "Total persons on frame: " << persons_.size();
}

}  // namespace SL
