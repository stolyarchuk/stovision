#ifndef HUMAN_PERSON_H
#define HUMAN_PERSON_H

#include "scene/human_face.h"

namespace SL {

class HumanPerson {
 public:
  using SharedPtr = std::shared_ptr<HumanPerson>;
  using Vector = std::vector<SharedPtr>;

  HumanPerson(HumanFace::SharedPtr);

  void SetFace(HumanFace::SharedPtr face) {
    face_ = face;
  }

  HumanFace::SharedPtr GetFace() const {
    return face_;
  }

 private:
  HumanFace::SharedPtr face_;
};

}  // namespace SL

#endif  // HUMAN_H
