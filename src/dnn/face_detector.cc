#include "face_detector.h"

namespace SL {

FaceDetector::FaceDetector() {
}

FaceDetector::~FaceDetector() {
}

OpenCVFaceDetector::OpenCVFaceDetector(const std::string& weights, const std::string& config) {
  cnn_ = cv::dnn::readNetFromTensorflow(weights, config);
  mean_val_ = cv::Scalar(104.0, 177.0, 123.0);
}

void OpenCVFaceDetector::Predict(Frame::SharedPtr frame) {
  auto& mat = frame->Mat();
  int frame_height = mat.rows;
  int frame_width = mat.cols;

  cv::Mat input_blob = cv::dnn::blobFromImage(mat, 1.0, cv::Size(300, 300), mean_val_, true, false);

  cnn_.setInput(input_blob, "data");
  cv::Mat detection = cnn_.forward("detection_out");

  if (detection.empty())
    return;

  cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

  for (int i = 0; i < detectionMat.rows; i++) {
    float confidence = detectionMat.at<float>(i, 2);

    if (confidence > 0.7f) {
      float x1 = detectionMat.at<float>(i, 3) * frame_width;
      float y1 = detectionMat.at<float>(i, 4) * frame_height;
      float x2 = detectionMat.at<float>(i, 5) * frame_width;
      float y2 = detectionMat.at<float>(i, 6) * frame_height;

      auto face = std::make_shared<HumanFace>(x1, y1, x2, y2);
      frame->AddFace(face);
    }
  }
}

DlibFaceDetector::DlibFaceDetector(const std::string& shape_path) : detector_(dlib::get_frontal_face_detector()) {
  dlib::deserialize(shape_path) >> sp_;
}

#include <chrono>
using namespace std::chrono;

void DlibFaceDetector::Predict(Frame::SharedPtr frame) {
  auto& mat = frame->Mat();
  const int frame_height = mat.rows;
  const int frame_width = mat.cols;

  int in_height = 300;
  int in_width = 0;

  if (!in_width)
    in_width = static_cast<int>((frame_width / static_cast<float>(frame_height)) * in_height);

  float scale_height = frame_height / static_cast<float>(in_height);
  float scale_width = frame_width / static_cast<float>(in_width);

  cv::Mat dlib_frame_small;
  resize(mat, dlib_frame_small, cv::Size(in_width, in_height));

  dlib::cv_image<dlib::bgr_pixel> dlib_frame(dlib_frame_small);
  std::vector<dlib::rectangle> detections = detector_(dlib_frame);

  for (auto& detection : detections) {
    auto shape = sp_(dlib_frame, detection);
    dlib::matrix<dlib::rgb_pixel> face_chip;
    dlib::extract_image_chip(dlib_frame, dlib::get_face_chip_details(shape, 150, 0.25), face_chip);

    float x1 = detection.left() * scale_width;
    float y1 = detection.top() * scale_height;
    float x2 = detection.right() * scale_width;
    float y2 = detection.bottom() * scale_height;

    auto face = std::make_shared<HumanFace>(x1, y1, x2, y2);
    frame->AddFace(face);
  }
}

}  // namespace SL
