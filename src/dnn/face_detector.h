#ifndef ABSTRACT_FACE_DETECTOR_H
#define ABSTRACT_FACE_DETECTOR_H

#include <dlib/clustering.h>
#include <dlib/dnn.h>
#include <dlib/image_io.h>
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/opencv.h>

#include "camera/frame.h"
#include "core/logger.h"
#include "scene/human_face.h"
#include "scene/human_person.h"

namespace SL {

class FaceDetector {
 public:
  using SharedPtr = std::shared_ptr<FaceDetector>;
  using UniquePtr = std::unique_ptr<FaceDetector>;

  FaceDetector();
  virtual ~FaceDetector();

  virtual void Predict(Frame::SharedPtr frame) = 0;
};

class OpenCVFaceDetector : public FaceDetector {
 public:
  OpenCVFaceDetector(const std::string& weights, const std::string& config);

  virtual void Predict(Frame::SharedPtr frame) override;

 private:
  cv::Scalar mean_val_;
  cv::dnn::Net cnn_;
};

class DlibFaceDetector : public FaceDetector {
 public:
  DlibFaceDetector(const std::string& shape_path);

  virtual void Predict(Frame::SharedPtr frame) override;

 private:
  dlib::frontal_face_detector detector_;
  dlib::shape_predictor sp_;
};

}  // namespace SL

#endif  // ABSTRACT_FACE_DETECTOR_H
