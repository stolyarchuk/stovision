#include "face_comparator.h"

#include "core/logger.h"

namespace SL {

FaceComparator::FaceComparator() {
}

FaceComparator::~FaceComparator() {
}

DlibFaceComparator::DlibFaceComparator(const std::string& net_path, const std::string& shape_path) {
  dlib::deserialize(net_path) >> cnn_;
  dlib::deserialize(shape_path) >> sp_;
}

void DlibFaceComparator::Predict(cv::Mat& mat, HumanFace::SharedPtr face) {
  cv::Mat face_mat = face->GetFaceMat(mat);
  cv::resize(face_mat, face_mat, cv::Size(150, 150));

  dlib::cv_image<dlib::bgr_pixel> face_dlib_img(face_mat);
  dlib::matrix<dlib::rgb_pixel> face_dlib_mat;
  dlib::assign_image(face_dlib_mat, face_dlib_img);

  /* Dlib face shaping
      auto face_rect = face->GetRawRect();
      dlib::rectangle dlib_rect(face_rect.tl().x, face_rect.tl().y, face_rect.br().x, face_rect.br().y);
      dlib::cv_image<dlib::bgr_pixel> face_dlib_img(mat);
      auto shape = sp_(face_dlib_img, dlib_rect);
      dlib::matrix<dlib::rgb_pixel> face_chip;
      dlib::extract_image_chip(face_dlib_img, dlib::get_face_chip_details(shape, 150, 0.25), face_chip);
      std::vector<dlib::matrix<dlib::rgb_pixel>> faces{face_chip};
  */

  std::vector<dlib::matrix<dlib::rgb_pixel>> faces{face_dlib_mat};
  std::vector<dlib::matrix<float, 0, 1>> face_descriptors = cnn_(faces);

  if (faces.empty()) {
    LOG(WARNING) << "No faces found in image!";
    return;
  }

  face->SetFeatures(face_descriptors.at(0));
}

}  // namespace SL
