#include "face_age_detector.h"

namespace SL {

AgeDetector::AgeDetector(const std::string& weights) {
  cnn_ = cv::dnn::readNetFromTensorflow(weights);
}

void AgeDetector::Predict(cv::Mat& mat, HumanFace::SharedPtr face) {
  std::lock_guard<std::mutex> locker(enroll_mutex_);

  cv::Mat face_rect = face->GetFaceWithMargin(mat);
  cv::resize(face_rect, face_rect, cv::Size(224, 224));
  cv::Mat input_blob = cv::dnn::blobFromImage(face_rect, 1.0, cv::Size(224, 224));

  cnn_.setInput(input_blob, "input_1");
  cv::Mat detection = cnn_.forward("pred_age/Softmax");

  if (detection.empty())
    return;

  double minVal, maxVal;
  cv::Point minLoc, maxLoc;

  cv::minMaxLoc(detection, &minVal, &maxVal, &minLoc, &maxLoc);

  if (maxLoc.x > -1 && maxLoc.x < 102)
    face->SetAge(maxLoc.x);
}

}  // namespace SL
