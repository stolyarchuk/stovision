#ifndef FACE_AGE_DETECTOR_H
#define FACE_AGE_DETECTOR_H

#include <memory>
#include <mutex>
#include <opencv2/dnn.hpp>

#include "camera/frame.h"
#include "scene/human_face.h"

namespace SL {

class AgeDetector {
 public:
  using SharedPtr = std::shared_ptr<AgeDetector>;
  using UniquePtr = std::unique_ptr<AgeDetector>;

  AgeDetector(const std::string&);

  void Predict(cv::Mat&, HumanFace::SharedPtr);

 private:
  cv::dnn::Net cnn_;
  std::mutex enroll_mutex_;
};

}  // namespace SL

#endif  // FACE_AGE_DETECTOR_H
