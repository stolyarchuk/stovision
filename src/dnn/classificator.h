#ifndef CLASSIFICATOR_H
#define CLASSIFICATOR_H

#include <boost/signals2.hpp>
#include <opencv2/dnn.hpp>
#include <queue>

#include "camera/frame.h"
#include "core/block.h"
#include "core/logger.h"
#include "core/work_queue.h"
#include "dnn/face_age_detector.h"
#include "dnn/face_comparator.h"
#include "dnn/face_detector.h"
#include "scene/human_face.h"

namespace SL {

struct Threshold {
  void Set(size_t n) {
    threshold_ = n;
  }

  bool operator()() {
    bool trigger = false;

    if (++current_ >= threshold_) {
      trigger = true;
      current_ = 0;
    }

    return trigger;
  }

 private:
  size_t threshold_;
  size_t current_;
};

class Classificator : public Block {
 public:
  using SharedPtr = std::shared_ptr<Classificator>;
  using UniquePtr = std::unique_ptr<Classificator>;
  using Block::Block;

  void Start() override;
  void Stop() override;

  void Enroll(Frame::SharedPtr frame);

 private:
  WorkQueue work_queue_;
  Threshold age_threshold_;

  FaceDetector::SharedPtr face_nn_;
  AgeDetector::SharedPtr age_nn_;
  FaceComparator::SharedPtr cmp_nn_;

  std::queue<std::future<void>> futures_;

  void ClearQueue() {
    std::queue<std::future<void>> empty;
    std::swap(futures_, empty);
  }

  void InitFaceDetector(const std::string& models_path);
  void InitAgeDetector(const std::string& models_path);
  void InitFaceComparator(const std::string& models_path);

  void DetectFaces(Frame::SharedPtr frame);
  void DetectFaceAges(Frame::SharedPtr frame, HumanFace::SharedPtr face);
  void CalcFaceFeatures(Frame::SharedPtr frame, HumanFace::SharedPtr face);
};

}  // namespace SL

#endif  // CLASSIFICATOR_H
