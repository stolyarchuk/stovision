#include "classificator.h"

#include <opencv2/core/utils/filesystem.hpp>
#include <opencv2/imgproc.hpp>

#include "core/utils.h"

namespace SL {

void Classificator::Start() {
  age_threshold_.Set(GetOpt<uint16_t>("threshold"));
  std::string models_path = GetOpt<std::string>("net");

  InitFaceDetector(models_path);
  InitAgeDetector(models_path);
  InitFaceComparator(models_path);

  work_queue_.Start(4);

  LOG(INFO) << "Classificator module started";
}

void Classificator::Stop() {
  work_queue_.WaitForCompletion();
}

void Classificator::Enroll(Frame::SharedPtr frame) {
  if (!futures_.empty())
    ClearQueue();

  DetectFaces(frame);

  //  std::vector<std::future<void>> calc_fututes;

  for (auto& face : frame->GetFaces()) {
    //    CalcFaceFeatures(frame, face);

    if (age_threshold_())
      DetectFaceAges(frame, face);
  }

  while (!futures_.empty()) {
    auto fut = std::move(futures_.front());
    fut.get();
    futures_.pop();
  }
}

void Classificator::InitFaceDetector(const std::string& models_path) {
  std::string detector = GetOpt<std::string>("detector");

  switch (_hash(detector.c_str())) {
    case "opencv"_: {
      std::string face_nn_weights =
          cv::utils::fs::join(models_path, "opencv_face_detector_uint8.pb");
      std::string face_nn_config =
          cv::utils::fs::join(models_path, "opencv_face_detector.pbtxt");
      face_nn_ =
          std::make_shared<OpenCVFaceDetector>(face_nn_weights, face_nn_config);
      break;
    }
    case "dlib"_: {
      std::string shape_path = cv::utils::fs::join(
          models_path, "shape_predictor_5_face_landmarks.dat");
      face_nn_ = std::make_shared<DlibFaceDetector>(shape_path);
      break;
    }
    default:
      throw std::invalid_argument("Bad face detector: " + detector);
  }
}

void Classificator::InitAgeDetector(const std::string& models_path) {
  std::string age_nn_weights =
      cv::utils::fs::join(models_path, "resnet_age_detector.pb");
  age_nn_ = std::make_shared<AgeDetector>(age_nn_weights);
}

void Classificator::InitFaceComparator(const std::string& models_path) {
  std::string net_path = cv::utils::fs::join(
      models_path, "dlib_face_recognition_resnet_model_v1.dat");
  std::string shape_path =
      cv::utils::fs::join(models_path, "shape_predictor_5_face_landmarks.dat");
  cmp_nn_ = std::make_shared<DlibFaceComparator>(net_path, shape_path);
}

void Classificator::DetectFaces(Frame::SharedPtr frame) {
  try {
    face_nn_->Predict(frame);

  } catch (std::exception& e) {
    LOG(ERROR) << e.what();
  }
}

void Classificator::DetectFaceAges(Frame::SharedPtr frame,
                                   HumanFace::SharedPtr face) {
  try {
    float margin = GetOpt<float>("margin");

    auto future = work_queue_.Enqueue(
        [&, frame](HumanFace::SharedPtr face) {
          auto& mat = frame->Mat();
          face->Margin(mat, margin);
          age_nn_->Predict(mat, face);
          face->DrawRect(mat);
        },
        face);

    futures_.push(std::move(future));

  } catch (std::exception& e) {
    LOG(ERROR) << e.what();
  }
}

void Classificator::CalcFaceFeatures(Frame::SharedPtr frame,
                                     HumanFace::SharedPtr face) {
  try {
    auto future = work_queue_.Enqueue(
        [&, frame](HumanFace::SharedPtr face) {
          auto& mat = frame->Mat();
          cmp_nn_->Predict(mat, face);
        },
        face);

    futures_.push(std::move(future));
  } catch (std::exception& e) {
    LOG(ERROR) << e.what();
  }
}
}  // namespace SL
