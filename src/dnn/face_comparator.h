#ifndef FACE_COMPARATOR_H
#define FACE_COMPARATOR_H

#include <memory>

#include <dlib/clustering.h>
#include <dlib/dnn.h>
#include <dlib/image_io.h>
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/opencv.h>

#include "scene/human_face.h"

namespace SL {

class FaceComparator {
 public:
  using SharedPtr = std::shared_ptr<FaceComparator>;
  using UniquePtr = std::unique_ptr<FaceComparator>;

  FaceComparator();
  virtual ~FaceComparator();

  virtual void Predict(cv::Mat&, HumanFace::SharedPtr) = 0;
};

class DlibFaceComparator : public FaceComparator {
 public:
  template <template <int, template <typename> class, int, typename> class block, int N, template <typename> class BN,
            typename SUBNET>
  using Residual = dlib::add_prev1<block<N, BN, 1, dlib::tag1<SUBNET>>>;
  template <template <int, template <typename> class, int, typename> class block, int N, template <typename> class BN,
            typename SUBNET>
  using ResidualDown =
      dlib::add_prev2<dlib::avg_pool<2, 2, 2, 2, dlib::skip1<dlib::tag2<block<N, BN, 2, dlib::tag1<SUBNET>>>>>>;

  template <int N, template <typename> class BN, int stride, typename SUBNET>
  using LayerBlock = BN<dlib::con<N, 3, 3, 1, 1, dlib::relu<BN<dlib::con<N, 3, 3, stride, stride, SUBNET>>>>>;
  template <int N, typename SUBNET>
  using Ares = dlib::relu<Residual<LayerBlock, N, dlib::affine, SUBNET>>;
  template <int N, typename SUBNET>
  using AresDown = dlib::relu<ResidualDown<LayerBlock, N, dlib::affine, SUBNET>>;
  template <typename SUBNET>
  using Alevel0 = AresDown<256, SUBNET>;
  template <typename SUBNET>
  using Alevel1 = Ares<256, Ares<256, AresDown<256, SUBNET>>>;
  template <typename SUBNET>
  using Alevel2 = Ares<128, Ares<128, AresDown<128, SUBNET>>>;
  template <typename SUBNET>
  using Alevel3 = Ares<64, Ares<64, Ares<64, AresDown<64, SUBNET>>>>;
  template <typename SUBNET>
  using Alevel4 = Ares<32, Ares<32, Ares<32, SUBNET>>>;
  using AnetType = dlib::loss_metric<dlib::fc_no_bias<
      128,
      dlib::avg_pool_everything<Alevel0<Alevel1<Alevel2<Alevel3<Alevel4<dlib::max_pool<
          3, 3, 2, 2, dlib::relu<dlib::affine<dlib::con<32, 7, 7, 2, 2, dlib::input_rgb_image_sized<150>>>>>>>>>>>>>;

  using SharedPtr = std::shared_ptr<DlibFaceComparator>;
  using UniquePtr = std::unique_ptr<DlibFaceComparator>;

  DlibFaceComparator(const std::string&, const std::string&);

  void Predict(cv::Mat&, HumanFace::SharedPtr) override;

 private:
  AnetType cnn_;
  dlib::shape_predictor sp_;
};

}  // namespace SL

#endif  // COMPARATOR_H
