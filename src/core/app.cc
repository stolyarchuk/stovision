#include "app.h"

#include "core/logger.h"

#include "camera/camera.h"
//#include "core/db.h"
#include "dnn/classificator.h"
#include "scene/scene.h"

namespace SL {

App::App() : io_() {}

App::~App() {}

void App::Configure(int argc, char** argv) {
  std::string full_path(argv[0]);
  size_t slash_pos = full_path.find_last_of('/');
  binary_name_ = full_path.substr(slash_pos + 1);

  opts_.ParseCommandLine(argc, argv);

  if (opts_.Count("help")) {
    throw OptionsHasHelp();
  } else if (opts_.Get<std::string>("net").empty()) {
    throw std::invalid_argument("Cant find models");
  }
}

void App::PrintUsage() {
  std::cout << "Usage: " << binary_name_ << " [options]\n\n" << opts_ << std::endl;
}

void App::InitLogging() {
  size_t level = opts_.Get<size_t>("verbose");  // TODO: remove this shit

  if (level > 4)
    LoggerConfig::kLevel = static_cast<SeverityLevel>(1);
  else
    LoggerConfig::kLevel = static_cast<SeverityLevel>(level);
}

void App::InitBlocks() {
  //  io_.Create<DB>("db", opts_);
  io_.Create<Classificator>("classificator", opts_);
  io_.Create<Scene>("scene", opts_);
  io_.Create<Camera>("camera", opts_);
}

void App::InitSignals() {
  auto camera = io_.GetBlock<Camera>("camera");
  auto scene = io_.GetBlock<Scene>("scene");
  auto classificator = io_.GetBlock<Classificator>("classificator");

  camera->ConnectToFrameCaptured(0, [&, classificator](Frame::SharedPtr frame) { classificator->Enroll(frame); });
  camera->ConnectToFrameCaptured(1, [&, scene](Frame::SharedPtr frame) { scene->Merge(frame); });
  camera->ConnectToFrameCaptured(2, [&, scene](Frame::SharedPtr frame) { scene->Render(frame); });
  scene->ConnectToKeyEscape(0, [&] { io_.Stop(); });
}

void App::Run() {
  io_.Start();
}

}  // namespace SL
