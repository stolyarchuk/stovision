#include "logger.h"

#include <boost/log/core/core.hpp>
#include <boost/log/trivial.hpp>

#include <boost/log/expressions.hpp>
#include <boost/log/expressions/formatters/date_time.hpp>
#include <boost/log/expressions/formatters/named_scope.hpp>

#include <boost/log/sinks/sink.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>

#include <boost/log/sources/severity_feature.hpp>
#include <boost/log/sources/severity_logger.hpp>

#include <boost/core/null_deleter.hpp>

#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/formatter_parser.hpp>

#include <boost/make_shared.hpp>
#include <boost/phoenix.hpp>
#include <boost/shared_ptr.hpp>

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace sinks = boost::log::sinks;
namespace attrs = boost::log::attributes;
namespace keywords = boost::log::keywords;

std::ostream& operator<<(std::ostream& strm, SeverityLevel level) {
  static const char* strings[] = {"debug", "info", "warn", "error", "fatal"};

  if (static_cast<std::size_t>(level) < sizeof(strings) / sizeof(*strings))
    strm << strings[level];

  else
    strm << static_cast<int>(level);

  return strm;
}

struct severity_tag;

static logging::formatting_ostream& operator<<(logging::formatting_ostream& strm,
                                               logging::to_log_manip<SeverityLevel, severity_tag> const& manip) {
  static const char* strings[] = {"debug", "info", "warn", "error", "fatal"};
  SeverityLevel level = manip.get();

  if (static_cast<std::size_t>(level) < sizeof(strings) / sizeof(*strings))
    strm << strings[level];

  else
    strm << static_cast<int>(level);

  return strm;
}

void write_header(sinks::text_file_backend::stream_type& file) {
  file << "<?xml version=\"1.0\"?>\n<log>\n";
}

void write_footer(sinks::text_file_backend::stream_type& file) {
  file << "</log>\n";
}

SeverityLevel LoggerConfig::kLevel = SeverityLevel::INFO;

BOOST_LOG_ATTRIBUTE_KEYWORD(line_id, "LineID", unsigned int)
BOOST_LOG_ATTRIBUTE_KEYWORD(timestamp, "TimeStamp", boost::posix_time::ptime)
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", SeverityLevel)
#ifdef SL_DEBUG
BOOST_LOG_ATTRIBUTE_KEYWORD(thread_id, "ThreadID", attrs::current_thread_id::value_type)

using ThreadType = attrs::current_thread_id::value_type;
using LogTagTid = logging::value_ref<attrs::current_thread_id::value_type, tag::thread_id>;

ThreadType::native_type get_native_thread_id(const LogTagTid& tid) {
  return tid ? tid->native_id() : 0;
}
#endif

BOOST_LOG_GLOBAL_LOGGER_INIT(logger, logger_t) {
  logger_t lg;
  logging::formatter formatter;
  lg.add_attribute("LineID", attrs::counter<unsigned int>(1));
  lg.add_attribute("TimeStamp", attrs::local_clock());
#ifdef SL_DEBUG
  lg.add_attribute("ThreadID", attrs::current_thread_id());
#endif

  typedef sinks::synchronous_sink<sinks::text_ostream_backend> text_sink;
  boost::shared_ptr<logging::core> core = logging::core::get();

  if (1 == ::getppid())
    formatter = expr::stream << "[" << std::left << std::setw(5) << expr::attr<SeverityLevel, severity_tag>("Severity")
#ifdef SL_DEBUG
                             << ":" << boost::phoenix::bind(&get_native_thread_id, thread_id.or_none())
#endif
                             << "] " << expr::smessage;

  else
    formatter = expr::stream << expr::format_date_time(timestamp, "%Y-%m-%d %H:%M:%S") << " [" << std::left
                             << std::setw(5) << expr::attr<SeverityLevel, severity_tag>("Severity")
#ifdef SL_DEBUG
                             << ":" << boost::phoenix::bind(&get_native_thread_id, thread_id.or_none())
#endif
                             << "] " << expr::smessage;

  boost::shared_ptr<text_sink> sink = boost::make_shared<text_sink>();
  sink->locked_backend()->add_stream(boost::shared_ptr<std::ostream>(&std::clog, boost::null_deleter()));
  sink->set_formatter(formatter);
  sink->set_filter(severity >= LoggerConfig::kLevel);

  core->add_sink(sink);

  return lg;
}
