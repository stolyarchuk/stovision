#ifndef BASE_BLOCK_H
#define BASE_BLOCK_H

#include <map>
#include <memory>
#include <thread>

#include "core/io.h"
#include "core/options.h"

namespace SL {

class Block : public std::enable_shared_from_this<Block> {
 public:
  using SharedPtr = std::shared_ptr<Block>;
  using UniquePtr = std::unique_ptr<Block>;
  using ThreadPtr = std::unique_ptr<std::thread>;

  Block(Io& io, Options& opts);

  virtual ~Block() = default;

  virtual void Start() = 0;
  virtual void Stop() = 0;

  template <typename OptionType>
  const OptionType GetOpt(const std::string& opt_name) const {
    return opts_.Get<OptionType>(opt_name);
  }

  template <typename BlockType>
  std::shared_ptr<BlockType> GetBlock(const std::string& module_name) {
    return io_.GetBlock<BlockType>(module_name);
  }

 private:
  Io& io_;
  Options& opts_;
};

}  // namespace SL

#endif  // BASE_BLOCK_H
