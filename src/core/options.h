#ifndef OPTIONS_H
#define OPTIONS_H

// #include <any>
#include <iostream>

#include <boost/program_options.hpp>

#include "logger.h"

namespace boost_opts = boost::program_options;

namespace SL {

class Options {
 public:
  Options();

  void ParseCommandLine(int argc, char** argv);
  bool Count(const std::string& name) const;

  template <typename T>
  T Get(const std::string& name) const {
    return opts_.at(name).as<T>();
  }

  inline friend std::ostream& operator<<(std::ostream& os, const Options& O) {
    return os << O.opts_desc_;
  }

 private:
  boost_opts::options_description opts_desc_;
  boost_opts::variables_map opts_;
};

}  // namespace SL

#endif  // OPTIONS_H
