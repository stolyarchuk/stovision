#include "options.h"

namespace SL {

Options::Options() {
  opts_desc_.add_options()                                                                                            //
      ("camera,c", boost_opts::value<int>()->default_value(0), "Camera No.")                                          //
      ("detector,d", boost_opts::value<std::string>()->default_value("opencv"), "Face Detector variant")              //
      ("help,h", "Print this help message and exit.")                                                                 //
      ("image-dir,i", boost_opts::value<std::string>()->default_value("."), "Images directory")                       //
      ("net,n", boost_opts::value<std::string>()->default_value(""), "Path to frozen DNNs")                           //
      ("margin,m", boost_opts::value<float>()->default_value(0.25f), "Face detection crop margin")                    //
      ("framerate,f", boost_opts::value<uint16_t>()->default_value(10), "Camera framerate (default: 10)")             //
      ("resolution,r", boost_opts::value<std::string>()->default_value("1280x720"), "Resolution(default: 1280x720)")  //
      ("threshold,t", boost_opts::value<uint16_t>()->default_value(10), "DNN propagation threshold (default: 10)")    //
      ("verbose,v", boost_opts::value<size_t>()->default_value(1), "Logging level (0..4)")                            //
      ("db_host", boost_opts::value<std::string>()->default_value("localhost"), "Database host")                      //
      ("db_port", boost_opts::value<uint16_t>()->default_value(5432), "Database port")                                //
      ("db_user", boost_opts::value<std::string>()->default_value("age"), "Database uesr")                            //
      ("db_password", boost_opts::value<std::string>()->default_value("age"), "Database password")                    //
      ("db_name", boost_opts::value<std::string>()->default_value("terminal"), "Database name");                      //
}

void Options::ParseCommandLine(int argc, char** argv) {
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, opts_desc_), opts_);
  boost::program_options::notify(opts_);
}

bool Options::Count(const std::string& name) const {
  return opts_.count(name);
}

}  // namespace SL
