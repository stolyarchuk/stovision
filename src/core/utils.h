#ifndef UTILS_H
#define UTILS_H

#include <cstddef>

#include <opencv2/opencv.hpp>

namespace SL {
constexpr unsigned int _hash(const char* str, int h = 0) {
  return !str[h] ? 5381 : (_hash(str, h + 1) * 33) ^ static_cast<unsigned int>(str[h]);
}

constexpr auto operator"" _(const char* p, std::size_t) {
  return _hash(p);
}
/*
class Color {
 public:
  static cv::Scalar Aliceblue;
  static cv::Scalar Antiquewhite;
  static cv::Scalar Aqua;
  static cv::Scalar Aquamarine;
  static cv::Scalar Azure;
  static cv::Scalar Beige;
  static cv::Scalar Bisque;
  static cv::Scalar Black;
  static cv::Scalar Blanchedalmond;
  static cv::Scalar Blue;
  static cv::Scalar Blueviolet;
  static cv::Scalar Brown;
  static cv::Scalar Burlywood;
  static cv::Scalar Cadetblue;
  static cv::Scalar Chartreuse;
  static cv::Scalar Chocolate;
  static cv::Scalar Coral;
  static cv::Scalar Cornflowerblue;
  static cv::Scalar Cornsilk;
  static cv::Scalar Crimson;
  static cv::Scalar Cyan;
  static cv::Scalar Darkblue;
  static cv::Scalar Darkcyan;
  static cv::Scalar Darkgoldenrod;
  static cv::Scalar Darkgray;
  static cv::Scalar Darkgreen;
  static cv::Scalar Darkgrey;
  static cv::Scalar Darkkhaki;
  static cv::Scalar Darkmagenta;
  static cv::Scalar Darkolivegreen;
  static cv::Scalar Darkorange;
  static cv::Scalar Darkorchid;
  static cv::Scalar Darkred;
  static cv::Scalar Darksalmon;
  static cv::Scalar Darkseagreen;
  static cv::Scalar Darkslateblue;
  static cv::Scalar Darkslategray;
  static cv::Scalar Darkslategrey;
  static cv::Scalar Darkturquoise;
  static cv::Scalar Darkviolet;
  static cv::Scalar Deeppink;
  static cv::Scalar Deepskyblue;
  static cv::Scalar Dimgray;
  static cv::Scalar Dimgrey;
  static cv::Scalar Dodgerblue;
  static cv::Scalar Firebrick;
  static cv::Scalar Floralwhite;
  static cv::Scalar Forestgreen;
  static cv::Scalar Fuchsia;
  static cv::Scalar Gainsboro;
  static cv::Scalar Ghostwhite;
  static cv::Scalar Gold;
  static cv::Scalar Goldenrod;
  static cv::Scalar Gray;
  static cv::Scalar Green;
  static cv::Scalar Greenyellow;
  static cv::Scalar Grey;
  static cv::Scalar Honeydew;
  static cv::Scalar Hotpink;
  static cv::Scalar Indianred;
  static cv::Scalar Indigo;
  static cv::Scalar Ivory;
  static cv::Scalar Khaki;
  static cv::Scalar Lavender;
  static cv::Scalar Lavenderblush;
  static cv::Scalar Lawngreen;
  static cv::Scalar Lemonchiffon;
  static cv::Scalar Lightblue;
  static cv::Scalar Lightcoral;
  static cv::Scalar Lightcyan;
  static cv::Scalar Lightgoldenrodyellow;
  static cv::Scalar Lightgray;
  static cv::Scalar Lightgreen;
  static cv::Scalar Lightgrey;
  static cv::Scalar Lightpink;
  static cv::Scalar Lightsalmon;
  static cv::Scalar Lightseagreen;
  static cv::Scalar Lightskyblue;
  static cv::Scalar Lightslategray;
  static cv::Scalar Lightslategrey;
  static cv::Scalar Lightsteelblue;
  static cv::Scalar Lightyellow;
  static cv::Scalar Lime;
  static cv::Scalar Limegreen;
  static cv::Scalar Linen;
  static cv::Scalar Magenta;
  static cv::Scalar Maroon;
  static cv::Scalar Mediumaquamarine;
  static cv::Scalar Mediumblue;
  static cv::Scalar Mediumorchid;
  static cv::Scalar Mediumpurple;
  static cv::Scalar Mediumseagreen;
  static cv::Scalar Mediumslateblue;
  static cv::Scalar Mediumspringgreen;
  static cv::Scalar Mediumturquoise;
  static cv::Scalar Mediumvioletred;
  static cv::Scalar Midnightblue;
  static cv::Scalar Mintcream;
  static cv::Scalar Mistyrose;
  static cv::Scalar Moccasin;
  static cv::Scalar Navajowhite;
  static cv::Scalar Navy;
  static cv::Scalar Oldlace;
  static cv::Scalar Olive;
  static cv::Scalar Olivedrab;
  static cv::Scalar Orange;
  static cv::Scalar Orangered;
  static cv::Scalar Orchid;
  static cv::Scalar Palegoldenrod;
  static cv::Scalar Palegreen;
  static cv::Scalar Paleturquoise;
  static cv::Scalar Palevioletred;
  static cv::Scalar Papayawhip;
  static cv::Scalar Peachpuff;
  static cv::Scalar Peru;
  static cv::Scalar Pink;
  static cv::Scalar Plum;
  static cv::Scalar Powderblue;
  static cv::Scalar Purple;
  static cv::Scalar Red;
  static cv::Scalar Rosybrown;
  static cv::Scalar Royalblue;
  static cv::Scalar Saddlebrown;
  static cv::Scalar Salmon;
  static cv::Scalar Sandybrown;
  static cv::Scalar Seagreen;
  static cv::Scalar Seashell;
  static cv::Scalar Sienna;
  static cv::Scalar Silver;
  static cv::Scalar Skyblue;
  static cv::Scalar Slateblue;
  static cv::Scalar Slategray;
  static cv::Scalar Slategrey;
  static cv::Scalar Snow;
  static cv::Scalar Springgreen;
  static cv::Scalar Steelblue;
  static cv::Scalar Tan;
  static cv::Scalar Teal;
  static cv::Scalar Thistle;
  static cv::Scalar Tomato;
  static cv::Scalar Turquoise;
  static cv::Scalar Violet;
  static cv::Scalar Wheat;
  static cv::Scalar White;
  static cv::Scalar Whitesmoke;
  static cv::Scalar Yellow;
  static cv::Scalar Yellowgreen;
};
*/
}  // namespace SL

#endif  // UTILS_H
