#include "work_queue.h"

namespace SL {

WorkQueue::WorkQueue() : stopped_(false), finish_work_(true) {
}

WorkQueue::~WorkQueue() {
  Abort();
}

void WorkQueue::Start(std::size_t total_threads) {
  if (total_threads < 1) {
    total_threads = std::thread::hardware_concurrency() + 1;
  }

  while (total_threads--) {
    worker_threads_.emplace_back(std::thread(&WorkQueue::StartWorker, this));
  }
}

void WorkQueue::Stop() {
  stopped_ = true;
  finish_work_ = false;
  condition_.notify_all();
}

void WorkQueue::StartWorker() {
  while (!stopped_ || (finish_work_ && !tasks_.empty())) {
    std::function<void()> task;

    {
      std::unique_lock<std::mutex> lock(queue_mutex_);
      condition_.wait(lock, [this] { return (stopped_ || !tasks_.empty()); });

      if (stopped_ && tasks_.empty())
        return;

      task = std::move(tasks_.front());
      tasks_.pop_front();
    }

    task();
  }
}

void WorkQueue::JoinAll() {
  std::vector<std::thread> workers;
  {
    std::lock_guard<std::mutex> lg(queue_mutex_);
    workers = std::move(worker_threads_);
  }
  for (auto& thread : workers) {
    thread.join();
  }
}

void WorkQueue::Abort() {
  stopped_ = true;
  finish_work_ = false;
  condition_.notify_all();
  JoinAll();

  {
    std::lock_guard<std::mutex> lg(queue_mutex_);
    tasks_.clear();
  }
}

void WorkQueue::WaitForCompletion() {
  Stop();
  JoinAll();
  assert(tasks_.empty());
}

}  // namespace SL
