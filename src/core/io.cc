#include "io.h"

#include "camera/camera.h"
#include "core/block.h"
#include "core/logger.h"

namespace SL {

Io::Io() : io_(), signals_(io_, SIGINT, SIGTERM) {
  signals_.async_wait([&](const boost::system::error_code& error, int signal_number) {
    if (error)
      return;

    Stop();

    LOG(INFO) << "App stopped by signal " << signal_number;
  });
}

Io::~Io() {
}

void Io::Start() {
  for (auto& block : blocks_) {
    block.second->Start();
  }

  StartThreads();
}

void Io::Stop() {
  for (auto& block : blocks_) {
    block.second->Stop();
  }

  if (!io_.stopped())
    io_.stop();
}

void Io::StartThreads() {
  //  size_t cpu_count = std::thread::hardware_concurrency();

  //  if (cpu_count == 0) {
  //    cpu_count = 1;
  //    LOG(WARNING) << "Cant detect number of CPU cores";
  //  } else if (cpu_count > 3)
  //    cpu_count /= 2;

  //  for (size_t i = 0; i < cpu_count; ++i) {
  //    auto thread = std::make_unique<std::thread>([&] {
  //      LOG(DEBUG) << "IO pool thread created";
  //      io_.run();
  //    });

  //    threads_.emplace_back(std::move(thread));
  //  }

  threads_.emplace_back(std::make_unique<std::thread>([&] { io_.run(); }));
  threads_.emplace_back(std::make_unique<std::thread>([&]() { GetBlock<Camera>("camera")->Loop(); }));

  for (auto& thread : threads_)
    if (thread->joinable())
      thread->join();
}

}  // namespace SL
