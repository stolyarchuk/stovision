#ifndef APP_H
#define APP_H

#include <memory>
#include <thread>
#include <vector>

#include "core/io.h"
#include "core/options.h"

namespace SL {

struct OptionsHasHelp : public std::exception {};

class App {
 public:
  App();
  ~App();

  void Configure(int argc, char** argv);
  void PrintUsage();

  void InitLogging();
  void InitBlocks();
  void InitSignals();

  void Run();

 private:
  Io io_;
  Options opts_;

  std::string binary_name_;
};

}  // namespace SL

#endif  // APP_H
