#pragma once

#include <iostream>
#include <iterator>
#include <regex>

#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/trivial.hpp>
#include <string>

//#define NUM_SEVERITY_LEVELS 5

inline std::string ComputeMethodName(const std::string& pretty_function) {
  std::string pretty = pretty_function;
  size_t brackets = pretty.find("(");

  if (brackets != std::string::npos)
    pretty.replace(pretty.begin() + static_cast<int>(brackets + 1), pretty.end(), 1, ')');

  return pretty;
}

#define __VERY_PRETTY__ ComputeMethodName(__PRETTY_FUNCTION__)

enum SeverityLevel { DEBUG = 0, INFO = 1, WARNING = 2, ERROR = 3, FATAL = 4 };

struct LoggerConfig {
  static SeverityLevel kLevel;
};

using logger_t = boost::log::sources::severity_logger_mt<SeverityLevel>;

BOOST_LOG_GLOBAL_LOGGER(logger, logger_t)

#ifdef SL_DEBUG
#define LOG(severity) BOOST_LOG_SEV(logger::get(), severity) << "[" << __VERY_PRETTY__ << ":" << __LINE__ << "] "
#else
#define LOG(severity) BOOST_LOG_SEV(logger::get(), severity)
#endif
