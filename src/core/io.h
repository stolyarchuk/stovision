#ifndef IO_H
#define IO_H

#include <functional>
#include <thread>

#include <boost/asio.hpp>
#include <boost/signals2.hpp>

#include "core/options.h"

namespace SL {

class Block;

class Io {
 public:
#ifdef MODERN_BOOST
  using AsioIoType = boost::asio::io_context;
#else
  using AsioIoType = boost::asio::io_service;
#endif

  Io();
  ~Io();

  void Start();
  void Stop();

  template <typename T>
  void Create(const std::string& name, Options& opts) {
    blocks_[name] = std::make_shared<T>(*this, opts);
  }

  template <typename M>
  std::shared_ptr<M> GetBlock(const std::string& module_name) {
    return blocks_.count(module_name) ? std::dynamic_pointer_cast<M>(blocks_.at(module_name))
                                      : throw std::runtime_error("No such module " + module_name);
  }

  template <typename F>
  void Post(const F& f) {
    io_.post(f);
  }

 private:
  AsioIoType io_;
  boost::asio::signal_set signals_;

  std::map<std::string, std::shared_ptr<Block>> blocks_;
  std::vector<std::unique_ptr<std::thread>> threads_;

  void StartThreads();
};

}  // namespace SL

#endif  // IO_H
