#ifndef WORK_QUEUE_H
#define WORK_QUEUE_H

#include <atomic>
#include <cassert>
#include <condition_variable>
#include <functional>
#include <future>
#include <memory>
#include <mutex>
#include <queue>
#include <stdexcept>
#include <thread>
#include <vector>

namespace SL {

class WorkQueue {
 public:
  WorkQueue();
  ~WorkQueue();

  template <class F, class... Args>
  auto Enqueue(F&& f, Args&&... args) -> std::future<typename std::result_of<F(Args...)>::type> {
    using return_type = typename std::result_of<F(Args...)>::type;

    auto task = std::make_shared<std::packaged_task<return_type()> >(
        std::bind(std::forward<F>(f), std::forward<Args>(args)...));

    std::future<return_type> res = task->get_future();
    {
      std::unique_lock<std::mutex> lock(queue_mutex_);

      if (stopped_)
        throw std::runtime_error("Enqueue on stopped WorkPool");

      tasks_.emplace_back([task]() { (*task)(); });
    }
    condition_.notify_one();
    return res;
  }

  void Start(std::size_t total_threads);
  void WaitForCompletion();

 private:
  std::vector<std::thread> worker_threads_;
  std::deque<std::function<void()> > tasks_;

  std::mutex queue_mutex_;
  std::condition_variable condition_;
  std::atomic_bool stopped_;
  std::atomic_bool finish_work_;

  void StartWorker();
  void JoinAll();
  void Abort();
  void Stop();
};

}  // namespace SL

#endif  // WORK_QUEUE_H
