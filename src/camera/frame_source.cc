#include "frame_source.h"

namespace SL {

void FrameSource::ConnectToFrameCaptured(int idx, std::function<void(Frame::SharedPtr)>&& func) {
  on_frame_captured_.connect(idx, std::move(func));
}

}  // namespace SL
