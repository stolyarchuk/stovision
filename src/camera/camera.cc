#include "camera.h"

namespace SL {

void Camera::Start() {
  ResolutionToSize();

  framerate_ = GetOpt<uint16_t>("framerate");
  capture_.open(GetOpt<int>("camera"));
  stopped_ = false;

  if (capture_.isOpened()) {
    capture_.set(cv::CAP_PROP_FRAME_WIDTH, width_);
    capture_.set(cv::CAP_PROP_FRAME_HEIGHT, height_);
    capture_.set(cv::CAP_PROP_AUTOFOCUS, 1);

    if (framerate_ > 0 && framerate_ < 25)
      capture_.set(cv::CAP_PROP_FPS, framerate_);

    framerate_ = capture_.get(cv::CAP_PROP_FPS);

    LOG(INFO) << "Camera module started"                                     //
              << "; framerate: " << framerate_                               //
              << "; resolution: " << capture_.get(cv::CAP_PROP_FRAME_WIDTH)  //
              << 'x' << capture_.get(cv::CAP_PROP_FRAME_HEIGHT);
  } else {
    stopped_ = true;
    LOG(WARNING) << "Camera module stopped";
  }
}

void Camera::Stop() {
  stopped_ = true;
}

void Camera::Loop() {
  LOG(DEBUG) << "Camera loop started";

  while (!stopped_) {
    auto frame = std::make_shared<Frame>();

    if (capture_.isOpened() && !stopped_)
      capture_ >> frame;

    if (!frame->Empty() && !stopped_) {
      on_frame_captured_(frame);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(1000 / framerate_));
  }
}

void Camera::ConnectToFrameCaptured(int idx, std::function<void(Frame::SharedPtr)>&& func) {
  on_frame_captured_.connect(idx, std::move(func));
}

void Camera::ResolutionToSize() {
  std::string resolution = GetOpt<std::string>("resolution");
  try {
    size_t delim_pos = resolution.find('x');

    std::string w = resolution.substr(0, delim_pos);
    std::string h = resolution.substr(delim_pos + 1, resolution.size() - delim_pos);

    width_ = std::stoul(w);
    height_ = std::stoul(h);
  } catch (std::exception& e) {
    LOG(WARNING) << e.what() << "; Bad resolution: " << resolution;
    width_ = 640;
    height_ = 480;
  }
}

}  // namespace SL
