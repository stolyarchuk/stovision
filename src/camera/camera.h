#ifndef CAMERA_H
#define CAMERA_H

#include <atomic>
#include <memory>

#include <boost/signals2.hpp>
#include <opencv2/opencv.hpp>

#include "camera/frame.h"
#include "camera/frame_source.h"
#include "core/block.h"
#include "core/logger.h"

namespace SL {

class Camera : public FrameSource {
 public:
  using SharedPtr = std::shared_ptr<Camera>;
  using UniquePtr = std::unique_ptr<Camera>;

  using FrameSource::FrameSource;

  void Start() override;
  void Stop() override;

  void Loop();
  void ConnectToFrameCaptured(int idx, std::function<void(Frame::SharedPtr)>&& func);

 private:
  std::atomic_bool stopped_;
  std::uint16_t framerate_;
  cv::VideoCapture capture_;

  boost::signals2::signal<void(Frame::SharedPtr)> on_frame_captured_;

  size_t width_;
  size_t height_;

  void ResolutionToSize();
};

}  // namespace SL

#endif  // CAMERA_H
