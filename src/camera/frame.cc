#include "frame.h"

namespace SL {

Frame::Frame() {
}

Frame::Frame(const cv::Mat& source) {
  mat_ = source.clone();
}

cv::Mat& Frame::Mat() {
  return mat_;
}

bool Frame::Empty() const {
  return mat_.empty();
}

void Frame::AddFace(HumanFace::SharedPtr face) {
  faces_.push_back(face);
}

bool Frame::HasFaces() const {
  return !faces_.empty();
}

const HumanFace::Vector& Frame::GetFaces() {
  return faces_;
}

cv::VideoCapture& operator>>(cv::VideoCapture& cap, Frame::SharedPtr frame) {
  return cap >> frame->Mat();
}

}  // namespace SL
