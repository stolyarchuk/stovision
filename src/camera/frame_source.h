#ifndef FRAME_SOURCE_H
#define FRAME_SOURCE_H

#include <boost/signals2/signal.hpp>

#include "camera/frame.h"
#include "core/block.h"

namespace SL {

class FrameSource : public Block {
 public:
  using SharedPtr = std::shared_ptr<FrameSource>;
  using UniquePtr = std::unique_ptr<FrameSource>;

  using Block::Block;

  virtual void Start() {
  }
  virtual void Stop() {
  }

  virtual void Present(Frame::SharedPtr) {
  }

  void ConnectToFrameCaptured(int idx, std::function<void(Frame::SharedPtr)>&& func);

 private:
  boost::signals2::signal<void(Frame::SharedPtr)> on_frame_captured_;
};

}  // namespace SL

#endif  // FRAME_SOURCE_H
