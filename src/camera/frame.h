#ifndef FRAME_H
#define FRAME_H

#include <memory>
#include <vector>

#include <opencv2/opencv.hpp>

#include "core/logger.h"
#include "scene/human_face.h"

namespace SL {

class Frame {
 public:
  using SharedPtr = std::shared_ptr<Frame>;
  using UniquePtr = std::unique_ptr<Frame>;

  Frame();
  Frame(const cv::Mat& source);

  cv::Mat& Mat();
  bool Empty() const;

  void AddFace(HumanFace::SharedPtr);
  bool HasFaces() const;
  const HumanFace::Vector& GetFaces();

 private:
  cv::Mat mat_;

  HumanFace::Vector faces_;
};

cv::VideoCapture& operator>>(cv::VideoCapture& cap, Frame::SharedPtr frame);

}  // namespace SL

#endif  // FRAME_H
