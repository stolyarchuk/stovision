#include <iostream>

#include "core/app.h"

int main(int argc, char** argv) {
  int rc = EXIT_SUCCESS;

  SL::App app;

  try {
    app.Configure(argc, argv);

    app.InitLogging();
    app.InitBlocks();
    app.InitSignals();

    app.Run();

  } catch (SL::OptionsHasHelp&) {
    app.PrintUsage();
  } catch (std::exception& err) {
    std::cerr << err.what() << std::endl;
    app.PrintUsage();
    rc = EXIT_FAILURE;
  }

  return rc;
}
